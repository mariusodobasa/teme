# Scrieți un program Python care să determine dacă într-o listă există duplicate.
# Cerințe:
# - nu se poate folosi nici o funcție built-in
# - problema trebuie rezolvată în cel puțin 3 moduri

lista = [1, 1, 2, 3, 3, 4, 4, 5, 6, 6, 7, 7]

lista_duplicate = []

for k in lista:
    if lista.count(k) > 1:
        lista_duplicate.append(k)

print("Lista duplicatelor este:", set(lista_duplicate))
