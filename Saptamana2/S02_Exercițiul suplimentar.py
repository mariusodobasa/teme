'''
Exercițiul 5

Scrieți un program Python care să determine dacă într-o listă există duplicate.

Cerințe:

nu se poate folosi nici o funcție built-in
problema trebuie rezolvată în cel puțin 3 moduri
Exercițiul suplimentar

Tuxy dorește să împlementeze un nou paint pentru consolă. În timpul dezvoltării proiectului s-a izbit de o problemă pe care nu o poate rezolva singur și a apelat la ajutorul tău. El dorește să adauge o unealtă care să permită umplerea unei forme închise.

Exemplu:

Pornim de la imaginea inițială reprezentată mai jos, trebuie să umplem formele în care se află "x":

  |-----*------|          |******------|         |************|
  |--x--*------|          |******------|         |************|
  |******------|  ----->  |******------|  -----> |************|
  |-----******-|          |-----******-|         |-----*******|
  |-----*---*--|          |-----*---*--|         |-----*---***|
  |-----*---*-x|          |-----*---*--|         |-----*---***|
'''

import os


def pozitia(pozitie, punct):
    # sus: rand -1, coloana + 0
    # jos: rand +1, coloana + 0
    # stanga: rand + 0, coloana -1
    # dreata: rand + 0, coloana +1
    if pozitie == 'SUS':
        rand = -1
        coloana = 0
    elif pozitie == 'JOS':
        rand = 1
        coloana = 0
    elif pozitie == 'STANGA':
        rand = 0
        coloana = -1
    elif pozitie == 'DREAPTA':
        rand = 0
        coloana = 1
    else:
        rand = 0
        coloana = 0
    return (punct[0] + rand, punct[1] + coloana)


def este_valid(imaginea, punctul):
    rand, coloana = punctul
    if rand < 0 or rand > len(imaginea):
        return False

    if coloana < 0 or coloana > len(imaginea[rand]):
        return False

    return True


def este_gol(imaginea, punct):
    rand, coloana = punct
    return imaginea[rand][coloana] == '-'


def umple_punct(imaginea, punct):
    rand, coloana = punct
    imaginea[rand][coloana] = '*'


def umple_forma(imaginea, punct):
    """Funcția primește reprezentarea imaginii și coordonatele unui
    punct.

    În cazul în care punctul se află într-o formă închisă trebuie să
    umple forma respectivă cu caracterul "*"
    """
    de_umplut = [punct]
    while de_umplut:
        punctul_curent = de_umplut.pop(0)
        print('De verificat:', de_umplut)
        print('Verificam punctul:', punctul_curent)
        afisare(imaginea)
        if este_gol(imaginea, punctul_curent):
            umple_punct(imaginea, punctul_curent)

        coordonate = (
            pozitia('SUS', punctul_curent),
            pozitia('JOS', punctul_curent),
            pozitia('STANGA', punctul_curent),
            pozitia('DREAPTA', punctul_curent),
        )

        for punct_vecin in coordonate:
            if este_valid(imaginea, punct_vecin) and este_gol(imaginea, punct_vecin):
                print('Adaugam in de umplut:', punct_vecin)
                de_umplut.append(punct_vecin)


def afisare(imaginea):
    print('   ', end='')
    for index in range(len(imaginea[0])):
        print(index % 10, end=' ')
    print()
    for index, rand in enumerate(imaginea):
        print(index, end=': ')
        for coloana in rand:
            print(coloana, end=' ')
        print()
    input('Apasă orice pentru a continua...')
    os.system('cls')


def main():
    os.system('cls')
    """  Main function docstring """
    imaginea = [
        ["-", "-", "-", "-", "-", "*", "-", "-", "-", "-", "-", "-"],
        ["-", "-", "-", "-", "-", "*", "-", "-", "-", "-", "-", "-"],
        ["-", "-", "-", "-", "-", "*", "-", "-", "-", "-", "-", "-"],
        ["*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "-"],
        ["-", "-", "-", "-", "-", "*", "-", "*", "-", "-", "*", "-"],
        ["-", "-", "-", "-", "-", "*", "-", "*", "-", "-", "*", "-"],
    ]
    afisare(imaginea)
    umple_forma(imaginea, (1, 3))
    print("Succes!")
    #umple_forma(imaginea, (5, 11))


main()
