''' Exercițiul 1

Se primesc două valori numerice (ca număr sau ca șir de caractere la alegere).
Scrieți un program Python care să verifice dacă fiecare cifră/caracter din prima valoare se regăsește
și în a doua valoare (și vice-versa).'''


sir1 = input("Introdu șirul 1: ")
sir2 = input("Introdu șirul 2: ")


def split(word):
    return [char for char in word]


lista1 = split(sorted(sir1))
lista2 = split(sorted(sir2))
lista_temp = []

a = 0
b = 0

dimensiune = 0
if len(lista1) == len(lista2):
    for i in range(int(len(lista1))):
        if lista2[i] in sir1:
            if lista2[i] not in lista_temp:
                lista_temp.append(lista2[i])
    else:
        a += 1
    for i in range(int(len(lista2))):
        if lista1[i] in sir2:
            if (lista1[i] not in lista_temp):
                lista_temp.append(lista1[i])

    else:
        b += 1
else:
    print("Listele au dimensiuni diferite")
    dimensiune = 1

lista_temp = sorted(lista_temp)

if lista1 == lista2:
    print("\nȘirurile sunt identice!")
elif (a == b) and len(lista_temp) != 0:
    print('\nAm găsit {} element(e) comune: {}"'.format(
        len(lista_temp), lista_temp))
elif dimensiune == 0 and lista_temp == []:
    print("\nȘirurile introduse sunt diferite.")

