# În laboratorul lui Tuxy toți cercetătorii au asignat un id de utilizator.
# Pentru fiecare cercetător se salvează într-o listă de fiecare dată când a deschis ușa
# (fie pentru a intra, fie pentru a ieși).
# Tuxy suspectează că cineva rămâne tot timpul după program și ar dori să scrie un
# script care să îi verifice teoria,
# dar nu a reușit pentru că algoritmul său era prea costisitor pentru sistem.

# Cerințe:

#    Găsește cercetătorul ce stă peste program după o singură parcurgere a listei
#    Găsește cercetătorul ce stă peste program după o singură parcurgere a listei
#    și fără a aloca memorie suplimentară.

# Notă: Există doar un singur cercetător care stă peste program

lista_intrari = [1, 2, 2, 3, 3, 3, 1, 4, 4, 5, 5, 7, 7, 8, 8, 9, 9]
lista_intrari.sort()
lista_muncitori = []


def muncitori(lista_intrari):
    for i in lista_intrari:
        if lista_intrari.count(i) % 2 != 0:
            if i not in lista_muncitori:
                lista_muncitori.append(i)
    return lista_muncitori


def muncitori2(lista_intrari):

    for i in range(len(lista_intrari)):
        for j in range(i+1, len(lista_intrari)):
            if lista_intrari[i] == lista_intrari[j]:
                break
    return lista_intrari


print("Nu pleaca: {} => {} utilizatori ".format(
    muncitori(lista_intrari), len(muncitori(lista_muncitori))))
