# Tuxy dorește să împlementeze un nou paint pentru consolă. În timpul dezvoltării proiectului s-a izbit de o problemă
# pe care nu o poate rezolva singur și a apelat la ajutorul tău. Aplicația ține un istoric al tuturor mișcărilor pe care
# le-a făcut utilizatorul într-o listă.

# Exemplu de date de intrare:
# istoric = [
#     'STÂNGA', 2,
#     'JOS', 2,
#     'DREAPTA', 5,
# ]
# Fișierul de mai sus ne spune că utilizatorul a mutat cursorul 2 căsuțe la stânga după care 2 căsuțe în jos, iar ultima
# acțiune a fost să poziționeze cursorul cu 5 căsuțe în dreapta față de ultima poziție. El dorește un utilitar care să îi
# spună care este distanța dintre punctul de origine (0, 0) și poziția curentă a cursorului.

istoric = [
    'STANGA', 2,
    'JOS', 2,
    'DREAPTA', 5,
]


def deplasare():
    X = 0
    Y = 0
    for i in range(0, len(istoric)-1):
        if istoric[i] == 'STANGA':
            X -= istoric[i + 1]
            print("Stanga:", X)

        elif istoric[i] == 'DREAPTA':
            X += istoric[i + 1]
            print("Dreapta:", X)
        elif istoric[i] == 'JOS':
            Y -= istoric[i + 1]
            print("Jos:", Y)

        elif istoric[i] == 'SUS':
            Y += istoric[i + 1]
            print("Sus:", Y)
        else:
            pass
    print("Coordonate: X,Y: ({}:{})".format(X, Y))


deplasare()
