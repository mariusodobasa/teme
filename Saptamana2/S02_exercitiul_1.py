''' Exercițiul 1
Scrieți un program Python care să primească un text și să afișeze pe ecran în ordine alfabetică toate cuvintele cu un număr impar de caractere și frecvența unei vocale egală cu 3.
Condiții: - cuvântul trebuie sa aibă număr impar de caractere; - cuvântul trebuie să conțină cel puțin o vocală cu numărul de apariții egal cu 3;
Cerințe: - trebuie să găsim toate cuvintele ce respectă condițiile de mai sus - trebuie să le afișăm pe ecran în ordine alfabetică'''

# Introducere text de la tastatura
text = input("Introdu un text: ")

# Text folosit pentru verificare cod
# text = "Programarea este dispunerea cronologica a unor miscări, operații actiuni sau activităti astfel incât in finalul perioadei să se realizeze o stare posibilă a unui sistem"

# Definire vocale
vocale = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]

# Definire listă finală
final_list = []

# Transformare textului introdus și ordonarea sa case-insensitive într-o nouă listă
lista = list(text.split())
ordonat = list(sorted(lista, key=lambda name: name.lower()))

# Funcție verificare reguli impuse. Paramentrii introduși in funcție se vor folosi doar dacă dorim afșiarea opțională a cuvintelor ce respectă regula:


def numara_vocale(cuvant, nr):

    # (1. verificare dacă cuvantul este impar și lungimea sa este mai mare ca 3)
    if (len(cuvant) % 2) and len(cuvant) > 2:

        # (2. verificare dacă cuvantul conține vocale din lista creată iniția)
        for v in vocale:

            # (3. verificare dacă cuvântul are frecvența unei vocale egală cu 3)
            if cuvant.lower().count(v) > 2 and cuvant.lower().count(v) < 4:

                # Afișare opțională a cuvintelor care respectă regula
                # print("Cuvântul '{}' are {} caractere și conține următoarele vocale:".format(cuvant,nr))
                # print(" - vocala '{}' de {} ori".format(v, cuvant.lower().count(v)))

                # Adăugare cuvânt găsit in lista finală
                final_list.append(cuvant)
                break


# Executare funcție de numărare vocale pentru toate cuvintele din textul introdus
for i in range(int(len(ordonat))):
    numara_vocale(ordonat[i], len(ordonat[i]))

# Afișare rezultate
if final_list != []:
    print("Lista cuvintelor, ordonată alfabetic, care respectă regula cerută sunt: {} ".format(final_list))
else:
    print("Nu există cuvinte care să respecte regula cerută în textul: '{}'" .format(text))
