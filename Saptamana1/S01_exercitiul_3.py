# Exercițiul 3 (20 puncte).
#
# Scrieți un program Python care să rezolve o ecuație de gradul 1. Ecuațiile de gradul 1 sunt de forma a * x + b = c.
# Valorile pentru a, b, c vor fi primite de la utilizator, iar programul va afișa valoarea lui x.

print("\nEcuație: a * x + b = c\n")

# Preluare valori de la tastatură:
a = float(input("Introdu valoare a: "))
b = float(input("Introdu valaore b: "))
c = float(input("Introdu valoare c: "))

# Afișare ecuație cu numerele introduse
print("\nRezolvare ecuație:\n{} * x + {} = {}".format(a, b, c))

# Rezolvare ecuație: a * x + b = c
x = (c-b) / a

# Afișare rezolvare ecuație
print("({} - {}) / {} = {}".format(c, b, a, x))
print("x =", x)
