# Suplimentar (30 puncte)

# Scrieți un program Python care să îi ofere utilizatorului să ghicească un număr cuprins
# în intervalul 0 - 100 din maxim 7 încercări.

# Notă: Pentru moment numărul poate să fie predefinit în cadrul programului. De exemplu:

# secret = 67
# La fiecare etapă de joc utilizatorul va trebui să propună un număr.
# Pentru fiecare număr primit programul va trebui să facă următoarele acțiuni:

# să verifice dacă valoarea primită este un număr în intervalul 0 - 100 în caz contrar va afișa pe ecran un mesaj de eroare
# să verifice etapa din joc în cazul în care utilizatorul a epuizat cele 7 încercări, jocul se va termina și utilizatorul va primi un mesaj corespunzător.
# să compare numărul cu valoarea aleasă (cu secret):
# dacă valorile sunt egale, jucătorul a câștigat jocul
# dacă valoarea aleasă este mai mică se va afișa pe ecran: Numărul este mai mare.
# dacă valoarea aleasă este mai mare se va afișa pe ecran: Numărul este mai mic.

# Versiune upgrade:
# - numarul ce trebuie ghicit random
# - adaugare nivele de dificultate
# - afisare interval minim si maxim dupa fiecare incercare

# Import librarie random
import random


def este_numar(text):
    if not text:
        return False
    if text.startswith('0'):
        return False
    for caracter in text:
        if not ord('0') <= ord(caracter) <= ord('9'):
            return False
    return True


def citeste_numar(text):
    while True:
        valoare = input(text)
        if not este_numar(valoare):
            print("Eroare! Te rog introdu o valoare validă!")
            continue
        break
    return int(valoare)


# Inițializare valoare random pentru variabila secret în intervalul 0 - 100
secret = random.randint(1, 100)

# Definire lista valori interval ghicire si initializare nr. incercari
lista = [1, 100]
incercari = 0

# Setare dificultate pe 3 nivele, inițializare valoare încercări joc


def verificare(incercari):
    while incercari > 0:
        # Prelure număr de la tastatură
        numar = int(citeste_numar(
            "\nIntroduceți un număr din intervalul {} - {}: ".format(min(lista), max(lista))))
        # Verificare dacă se respectă intervalul de numere stabilit
        if numar > 100 or numar < 0:
            print("Eroare! Numărul este în afara intervalulul stabilit! Alegeti un numar intre {} și {}.".format(
                min(lista), max(lista)))
        elif numar > secret and numar < lista[1]:
            lista[1] = numar
        elif numar < secret and numar > lista[0]:
            lista[0] = numar

        # Verificare dacă numărul introdus este egal cu variabila secret și afișare mesaj
        if numar == secret:
            print("Felicitări! Ai ghicit! Numărul era: {}".format(secret))
            input("Apasa Enter pentru a iesi...")
            break

        # Afișare mesaj cu numărul de încercări și atenționare dacă acesta este mai mic sau mai mare
        elif incercari > 0:
            if numar > secret:
                print("Numărul căutat este mai mic.\nÎncercări rămase: {}". format(
                    incercari-1))
                incercari -= 1

                # Dupa epuizarea celor 7 incercari se afiseaza numarul secret
                if incercari == 0:
                    print("\nÎmi pare rău! nu ai ghicit!\nNumărul secret era:", secret)
                    break
            elif numar < secret:
                print("Numărul căutat este mai mare.\nÎncercări rămase: {}".format(
                    incercari-1))
                incercari -= 1

                # Dupa epuizarea celor 7 incercari se afiseaza numarul secret
                if incercari == 0:
                    print("\nÎmi pare rău! nu ai ghicit!\nNumărul secret era:", secret)
                    input("Apasa Enter pentru a iesi...")
                    break


def setare_dificultate():
    global incercari
    global dificultate
    dificultate = int(citeste_numar(
        "Nivel: "))

    if (dificultate > 3) and (dificultate < 1):
        print("Nivel de dificultate invalid!")
    elif dificultate == 1:
        incercari = 10
        print("\nNivel selectat: [ușor]")
        print("Încercări rămase:", incercari)
        verificare(incercari)
    elif dificultate == 2:
        incercari = 7
        print("\nNivel selectat: [normal]")
        print("Încercări rămase:", incercari)
        verificare(incercari)
    elif dificultate == 3:
        incercari = 5
        print("\nNivel selectat: [greu]")
        print("Încercări rămase:", incercari)
        verificare(incercari)
    else:
        print("Eroare! Nivelul selectat trebuie să fie 1,2 sau 3")
        main()


def main():
    setare_dificultate()

    return


print(
    "\nGhicește un număr între 1 și 100!\nNivel dificultate:\n[1] - ușor - 10 încercări\n[2] - normal - 7 încercări\n[3] - greu - 5 încercări\n")
main()
