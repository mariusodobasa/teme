# Exercițiul 2 (20 puncte)

# Scrieți un program Python care să determine valoarea maximă și valoarea
# minimă dintre 3 valori primite de la tastatură.

# Definire listă și vaiabile inițiale

import os

lista = []
numere = 3  # setare număr valori preluate de la tastatură


def este_numar(text):
    if not text:
        return False
    if text.startswith('0'):
        return False
    for caracter in text:
        if not ord('0') <= ord(caracter) <= ord('9'):
            return False
    return True


def citeste_numar(text):
    while True:
        valoare = input('Introduceti valoarea {}: '.format(text))
        if not este_numar(valoare):
            print('Valoare invalida. Incearca din nou!')
            continue
        break
    return int(valoare)


def main():
    # numere = int(input("Câte valori vrei să introduci: "))
    # Preluare numere de la tastatură și adăugare în lista creată inițial
    # atât timp cât variabila "i" este diferită de variabila "numere"
    i = 0
    while i != numere:
        a = citeste_numar(i+1)
        # a = int(input("Preluare număr {}: ".format(i+1)))
        lista.append(a)
        i += 1

    # Afișare listă preluată de la tastatură
    print("\nListă: {}".format(lista), "\n")

    # Afișare valoare maximă și minimă
    print("Valoare maximă listă: {}".format(max(lista)))
    print("Valoare minimă listă: {}".format(min(lista)))

    os.system("pause")


if __name__ == "__main__":
    main()
