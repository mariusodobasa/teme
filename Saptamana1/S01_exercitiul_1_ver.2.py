# Exercițiul 1

# Scrieți un program Python care să interschimbe două valori existente în program.

prima_valoare = 10
a_doua_valoare = 20

# Interschimare valori inițiale fără o variabilă temporară
prima_valoare, a_doua_valoare = a_doua_valoare, prima_valoare

# Afișare valori interschimbate la terminal
print(prima_valoare)  # 20
print(a_doua_valoare)  # 10
