# Exercițiul 3 (20 puncte).
#
# Scrieți un program Python care să rezolve o ecuație de gradul 1. Ecuațiile de gradul 1 sunt de forma a * x + b = c.
# Valorile pentru a, b, c vor fi primite de la utilizator, iar programul va afișa valoarea lui x.

import os
print("\nEcuație: a * x + b = c\n")


def este_numar(text):
    if not text:
        return False
    if text.startswith('0'):
        return False
    for caracter in text:
        if not ord('0') <= ord(caracter) <= ord('9'):
            return False
    return True


def citeste_numar(litera):
    while True:
        valoare = input('Introduceti valoare {}: '.format(litera))
        if not este_numar(valoare):
            print('Valoare invalida. Incearca din nou!')
            continue
        break
    return int(valoare)


def main():
    # Preluare valori de la tastatură:
    a = float(citeste_numar("a"))
    b = float(citeste_numar("b"))
    c = float(citeste_numar("c"))

    # Afișare ecuație cu numerele introduse
    print("\nRezolvare ecuație:\n{} * x + {} = {}".format(a, b, c))

    # Rezolvare ecuație: a * x + b = c
    x = (c-b) / a

    # Afișare rezolvare ecuație
    print("({} - {}) / {} = {}".format(c, b, a, x))
    print("x =", x)

    os.system("pause")


if __name__ == "__main__":
    main()
