# Teme Wantsome - Curs Python

**Saptamana 1 - Temă Noțiuni introductive**

> Exercițiul 1 (20 \*puncte)

Scrieți un program Python care să interschimbe două valori existente în program.

De exemplu pentru următoarele etichete:

```
prima_valoare = 10
a_doua_valoare = 20
după execuția programului

print(prima_valoare)  # 20
print(a_doua_valoare) # 10
```

> Exercițiul 2 (20 puncte)

Scrieți un program Python care să determine valoarea maximă și valoarea minimă dintre 3 valori primite de la tastatură.

> Exercițiul 3 (20 puncte).

Scrieți un program Python care să rezolve o ecuație de gradul 1. Ecuațiile de gradul 1 sunt de forma a \* x + b = c. Valorile pentru a, b, c vor fi primite de la utilizator, iar programul va afișa valoarea lui x.

> Exercițiul 4 (20 puncte)

Scrieți un program Python care să determine dacă un număr este par.

> Exercițiul 5 (20 puncte)

Scrieți un program Python care să verifice dacă un număr este prim.

Un număr este prim dacă acesta are doar doi divizori (1 și pe el însuși). O să numim un divizor un număr la care numărul nostru se împarte exact, sau cu alte cuvinte restul împărțirii numărului la divizor este 0.

> Suplimentar (30 puncte)

Scrieți un program Python care să îi ofere utilizatorului să ghicească un număr cuprins în intervalul 0 - 100 din maxim 7 încercări.

Notă: Pentru moment numărul poate să fie predefinit în cadrul programului. De exemplu:

secret = 67
La fiecare etapă de joc utilizatorul va trebui să propună un număr. Pentru fiecare număr primit programul va trebui să facă următoarele acțiuni:

- să verifice dacă valoarea primită este un număr în intervalul 0 - 100
- în caz contrar va afișa pe ecran un mesaj de eroare
- să verifice etapa din joc
- în cazul în care utilizatorul a epuizat cele 7 încercări, jocul se va termina și utilizatorul va primi un mesaj corespunzător.
- să compare numărul cu valoarea aleasă (cu secret):
- dacă valorile sunt egale, jucătorul a câștigat jocul
- dacă valoarea aleasă este mai mică se va afișa pe ecran: Numărul este mai mare.
- dacă valoarea aleasă este mai mare se va afișa pe ecran: Numărul este mai mic.
