# Exercițiul 4 (20 puncte)

# Scrieți un program Python care să determine dacă un număr este par.

# Preluare număr de la tastatură
numar = int(input("Introdu un număr: "))

# Verificare dacă restul împarțirii la 2 a numarului introdus este egal cu 0
if (numar % 2) == 0:
    print("Numărul {} este par".format(numar))
else:
    print("Numărul {} este impar".format(numar))
