# Exercițiul 2 (20 puncte)

# Scrieți un program Python care să determine valoarea maximă și valoarea
# minimă dintre 3 valori primite de la tastatură.

# Definire listă și vaiabile inițiale
lista = []
i = 0
numere = 3  # setare număr valori preluate de la tastatură

# Preluare numere de la tastatură și adăugare în lista creată inițial
# atât timp cât variabila "i" este diferită de variabila "numere"

while i != numere:
    a = int(input("Preluare număr {}: ".format(i+1)))
    lista.append(a)
    i += 1

# Afișare listă preluată de la tastatură
print("\nListă: {}".format(lista), "\n")

# Afișare valoare maximă și minimă
print("Valoare maximă listă: {}".format(max(lista)))
print("Valoare minimă listă: {}".format(min(lista)))
