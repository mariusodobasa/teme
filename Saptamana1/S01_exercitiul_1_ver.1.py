# Exercițiul 1 (20 *puncte)
# Scrieți un program Python care să interschimbe două valori existente în program.
# Valori inițiale

prima_valoare = 10
a_doua_valoare = 20

# Interschimare valori inițiale cu ajutorul unei variabile temporare
valoare_temp = a_doua_valoare
a_doua_valoare = prima_valoare
prima_valoare = valoare_temp

# Afișare valori interschimbate terminal
print(prima_valoare)
print(a_doua_valoare)