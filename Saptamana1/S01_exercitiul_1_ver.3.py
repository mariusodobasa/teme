# Exercițiul 1 (20 *puncte)
# Scrieți un program Python care să interschimbe două valori existente în program.


def este_numar(text):
    if not text:
        return False
    if text.startswith('0'):
        return False
    for caracter in text:
        if not ord('0') <= ord(caracter) <= ord('9'):
            return False
    return True


def citeste_numar(text):
    while True:
        valoare = input('Introduceti valoarea {}: '.format(text))
        if not este_numar(valoare):
            print('Valoare invalida. Incearca din nou!')
            continue
        break
    return int(valoare)


def main():
    prima_valoare = citeste_numar(1)
    a_doua_valoare = citeste_numar(2)

    # Interschimare valori inițiale fără o variabilă temporară
    prima_valoare, a_doua_valoare = a_doua_valoare, prima_valoare

    # Afișare valori interschimbate la terminal
    print("\nDupa interschimbare valorile sunt:")
    print("Valoarea 1 este: {}". format(prima_valoare))
    print("Valoarea 2 este: {}". format(a_doua_valoare))


main()
