# Suplimentar (30 puncte)

# Scrieți un program Python care să îi ofere utilizatorului să ghicească un număr cuprins
# în intervalul 0 - 100 din maxim 7 încercări.

# Notă: Pentru moment numărul poate să fie predefinit în cadrul programului. De exemplu:

# secret = 67
# La fiecare etapă de joc utilizatorul va trebui să propună un număr.
# Pentru fiecare număr primit programul va trebui să facă următoarele acțiuni:

# să verifice dacă valoarea primită este un număr în intervalul 0 - 100 în caz contrar va afișa pe ecran un mesaj de eroare
# să verifice etapa din joc în cazul în care utilizatorul a epuizat cele 7 încercări, jocul se va termina și utilizatorul va primi un mesaj corespunzător.
# să compare numărul cu valoarea aleasă (cu secret):
# dacă valorile sunt egale, jucătorul a câștigat jocul
# dacă valoarea aleasă este mai mică se va afișa pe ecran: Numărul este mai mare.
# dacă valoarea aleasă este mai mare se va afișa pe ecran: Numărul este mai mic.

# Import librarie random
import random

# Inițializare valoare random pentru variabila secret în intervalul 0 - 100
secret = random.randint(0, 100)

# Definire și inițializare valoare încercări joc
incercari = 7

# Atat timp cat numarul de incercari este mai de 7 se va rula bucla while
while incercari > 0:

    # Prelure număr de la tastatură
    numar = int(input("\nIntroduceți un număr în intervalul 0 - 100: "))

    # Verificare dacă se respectă intervalul de numere stabilit
    if numar > 100 or numar < 0:
        print("Eroare! Numărul este în afara intervalulul stabilit! Alegeti un numar intre 0 și 100.")

    # Verificare dacă numărul introdus este egal cu variabila secret și afișare mesaj
    if numar == secret:
        print("Felicitări! Ai ghicit! Numărul era: {}".format(secret))
        break

    # Afișare mesaj cu numărul de încercări și atenționare dacă acesta este mai mic sau mai mare
    elif incercari > 0:
        if numar > secret:
            print("Numărul căutat este mai mic.\nÎncercări rămase: {}". format(
                incercari-1))
            incercari -= 1

            # Dupa epuizarea celor 7 incercari se afiseaza numarul secret
            if incercari == 0:
                print("\nÎmi pare rău! nu ai ghicit!\nNumărul secret era:", secret)
                break
        elif numar < secret:
            print("Numărul căutat este mai mare.\nÎncercări rămase: {}".format(
                incercari-1))
            incercari -= 1

            # Dupa epuizarea celor 7 incercari se afiseaza numarul secret
            if incercari == 0:
                print("\nÎmi pare rău! nu ai ghicit!\nNumărul secret era:", secret)
                break
