'''
Exercițiul 5 (20 puncte)

Scrieți un program Python care să verifice dacă un număr este prim.
Un număr este prim dacă acesta are doar doi divizori (1 și pe el însuși).
O să numim un divizor un număr la care numărul nostru se împarte exact,
sau cu alte cuvinte restul împărțirii numărului la divizor este 0.'''

# Preluare număr de la tastatură
numar = int(input("Introduceți un număr: "))

# Verificare dacă numărul introdus este mai mare ca 1
if numar > 1:
    # Buclă verificare divizori
    for i in range(2, numar + 1):
        # Verificare daca restul împărțirii numărului la divizor este 0
        if numar % i == 0 and i != numar and i != 1:
            # Afisare mesaj cand numarul introdus nu este prim
            print("Numărul {} nu este prim!".format(numar))
            break
    else:
        # Afisare mesaj cand numarul introdus este prim
        print("Numărul {} este prim!".format(numar))

else:

    # Afisare messaj când numarul introdus este egal cu 1 sau este negativ.
    if numar == 1:
        print("Numărul 1 nu este prim!")
    else:
        print("Numărul {} este negativ!".format(numar))
