# Exercițiul 5 (20 puncte)

# Scrieți un program Python care să verifice dacă un număr este prim.

# Un număr este prim dacă acesta are doar doi divizori (1 și pe el însuși).
# O să numim un divizor un număr la care numărul nostru se împarte exact,
# sau cu alte cuvinte restul împărțirii numărului la divizor este 0.

# Preluare număr de la tastatură


def este_numar(text):
    if not text:
        return False
    if text.startswith('0'):
        return False
    for caracter in text:
        if not ord('0') <= ord(caracter) <= ord('9'):
            return False
    return True


def citeste_numar():
    while True:
        valoare = input('Introduceti un numar: ')
        if not este_numar(valoare):
            print('Valoare invalida. Incearca din nou!')
            continue
        break
    return int(valoare)


# functia functioneaza si cu o valoare introdusa de la tastatura dar si cu una introdusa ca argument ex: prim(4)
def prim(nr=""):
    if nr == "":
        numar = int(citeste_numar())
    else:
        numar = int(nr)

    # Verificare dacă numărul introdus este mai mare ca 1
    if numar > 1:
        # Verificare divizori
        for i in range(2, numar + 1):
            # Verificare daca restul împărțirii numărului la divizor este 0
            if numar % i == 0 and i != numar and i != 1:
                # Afisare mesaj cand numarul introdus nu este prim
                # print(i)
                print("Numărul {} nu este prim!".format(numar))
                return True
                break
        else:
            # print(i)
            # Afisare mesaj cand numarul introdus este prim
            print("Numărul {} este prim!".format(numar))
    else:
        # Afisare messaj când numarul introdus este egal cu 1 sau este negativ.
        if numar == 1:
            print("Numărul 1 nu este prim!")
            return False
        else:
            print("Numărul/Caracterul introus este invalid!")
            return False


if __name__ == "__main__":
    prim()
