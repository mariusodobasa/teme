# Exercițiul 4 (20 puncte)
# Scrieți un program Python care să determine dacă un număr este par.
# Preluare număr de la tastatură


def este_numar(text):
    if not text:
        return False
    if text.startswith('0'):
        return False
    for caracter in text:
        if not ord('0') <= ord(caracter) <= ord('9'):
            return False
    return True


def citeste_numar():
    while True:
        valoare = input('Introduceti un numar: ')
        if not este_numar(valoare):
            print('Valoare invalida. Incearca din nou!')
            continue
        break
    return int(valoare)


def main():
    numar = int(citeste_numar())

    # Verificare dacă restul împarțirii la 2 a numarului introdus este egal cu 0
    if (numar % 2) == 0:
        print("Numărul {} este par".format(numar))
    else:
        print("Numărul {} este impar".format(numar))


if __name__ == "__main__":
    main()
