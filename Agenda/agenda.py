from email_validator import validate_email, EmailNotValidError
from itertools import cycle
import json
import os
import tkinter as tk
import tkinter.messagebox
from tkinter import Button, Frame
from tkinter import font as tkfont


class Agenda:

    def __init__(self, database='agenda.json'):
        self._contacte = []
        self._lista_contacte = []
        self._lista_cautare = []
        self._database = database
        self._incarc()

    def _afisare_contact(self, contact):
        # print('Nume: %s' % contact.nume_complet)
        # print('Numar de telefon: %s' % contact.numar_de_telefon)
        # print('E-Mail: %s' % contact.email)
        # print()
        return (contact._nume, contact._prenume, contact._numar_de_telefon, contact._email)
        # return contact

    def _salvez(self):
        continut_agenda = []
        for contact in self._contacte:
            continut_agenda.append(contact.to_dict())

        with open(self._database, 'w') as file_handler:
            file_handler.write(json.dumps(
                continut_agenda, indent=4, sort_keys=False))

    def _incarc(self):
        if not os.path.isfile(self._database):
            return

        with open(self._database, 'r') as file_handler:
            continut = file_handler.read()

        try:
            data = json.loads(continut)
        except json.decoder.JSONDecodeError:
            return

        for contact_dict in data:
            contact = Contact.from_dict(contact_dict)
            self._contacte.append(contact)
        return continut

    def _sterge(self, contact):
        if not isinstance(contact, Contact):
            print('Agenda asteapta un contact.')
            print('Am primit %s' % contact)
            return False

        if contact in self._contacte:
            self._contacte.remove(contact)
        return True

    def _cautare_id(self, id):

        for contact in self._contacte:
            field_value = getattr(contact, "id")
            if field_value == id:
                return contact

    def _cautare(self, limit=1, **criterii):
        criterii_valide = Contact.FIELDS
        for criteriu in criterii:
            if criteriu not in criterii_valide:
                print("Criteriul %s nu este valid." % criteriu)
                return
        rezultat = []
        for contact in self._contacte:
            for criteriu, valoare in criterii.items():
                field_value = getattr(contact, criteriu)
                # Atentie: O sa functioneze doar pe siruri de caracter
                # Important pentru ca trebuie sa fim siguri ca
                # metoda .validate intoarce True pentru contactul curent
                if str(valoare.lower()) not in str(field_value.lower()):
                    break
            else:
                rezultat.append(contact)
        return rezultat

    def incarc(self):
        Agenda._incarc(self)

    def adauga(self, contact):
        if not isinstance(contact, Contact):
            print('Agenda asteapta un contact.')
            print('Am primit %s' % type(contact))
            return False
        self._contacte.append(contact)
        self._salvez()

    def cauta(self, **criterii):
        for index, contact in enumerate(self._cautare(**criterii)):
            self._afisare_contact(contact)

    def listeaza_cautare(self):
        for index, contact in enumerate(self._contacte):
            self._lista_cautare.append(contact.id_complet)
        return self._lista_cautare

    def listeaza(self):
        for index, contact in enumerate(self._contacte):
            self._lista_contacte.append(contact.id_complet)
        return self._lista_contacte

    def listeaza_pozitie(self, pozitie):
        for index, contact in enumerate(self._contacte):
            self._lista_contacte.append(contact.id_complet)
        print(self._contacte[pozitie].get())
        return self._lista_contacte

    def sterge(self, id):
        self._contacte.pop(int(id))
        self._salvez()
        self._incarc()


class Contact:

    FIELDS = ('nume', 'prenume', 'email', 'numar_de_telefon')
    CAN_BE_UPDATED = ('nume', 'prenume', 'email', 'numar_de_telefon')

    def __init__(self, nume, prenume, numar_de_telefon, email):

        self._nume = nume
        self._prenume = prenume
        self._numar_de_telefon = numar_de_telefon
        self._email = email

    @property
    def id_complet(self):
        contact = "%s %s %s" % (
            self.nume_complet, self.numar_de_telefon, self._email)
        return list(contact.split(" "))

    @property
    def nume_complet(self):
        return "%s %s" % (self._nume, self._prenume)

    @property
    def listeaza_tot(self):
        return "%s %s | \t %s | \t %s" % (self._prenume, self._nume, self._numar_de_telefon, self._email)

    @property
    def nume(self):
        return self._nume

    @nume.setter
    def nume(self, value):
        self._nume = value

    @property
    def prenume(self):
        return self._prenume

    @prenume.setter
    def prenume(self, value):
        self._prenume = value

    @property
    def numar_de_telefon(self):
        return self._numar_de_telefon

    @numar_de_telefon.setter
    def numar_de_telefon(self, value):
        self._numar_de_telefon = value

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, value):
        self._email = value

    def to_dict(self):
        data = {}
        for field in self.FIELDS:
            data[field] = getattr(self, field)
        return data

    @classmethod
    def from_dict(cls, raw_data):
        data = {}
        for field in cls.FIELDS:
            if field not in raw_data:
                print('Date insuficiente pentru a crea o instanta.')
                return None
            data[field] = raw_data[field]
        # return Contact(**data)
        # Stim cum o sa arate data:
        # {'nume': '', prenume: '', email: '', numar_de_telefon: ''}
        # **data -> key: value o sa devina key=value ca argumente numite
        # in apelul constructorului
        # print('Am primit datele: %s' % data)
        instance = cls(**data)
        # print('Am obtinut %s' % instance)
        return instance

    def valideaza(self):
        for valoare in (self._nume, self._prenume, self._numar_de_telefon,
                        self._email):
            if not isinstance(valoare, str):
                print('%s nu este sir de caractere' % valoare)
                return False
        return True

    def actualizeaza(self, **kwargs):
        agenda = Agenda()
        a = MultiListbox()
        print("Ai selectat:", lista_contacte.curselection())

        print("Modificari: {} pentru {}".format(id, kwargs))
        for key, value in kwargs.items():
            if key not in self.CAN_BE_UPDATED:
                print("Proprietatea %s nu poate fi modificata." % key)
                continue
            setattr(self, key, value)
        agenda._salvez()


class AgendaApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.title_font = tkfont.Font(
            family='Helvetica', size=14, weight="bold")
        container = tk.Frame(self)
        container.pack(side="left", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (Setari, Administrare, Adauga, Modifica, Despre, Ajutor):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("Administrare")

    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.tkraise()

    def rama(self, nume, xrelx=0.010, xrely=0.010, xrelheight=0.95, xrelwidth=0.980):

        self.TFrame1 = tk.LabelFrame(self, text=" " + nume + " ")
        self.TFrame1.place(relx=xrelx, rely=xrely,
                           relheight=xrelheight, relwidth=xrelwidth)
        self.TFrame1.configure(relief='ridge')
        self.TFrame1.configure(borderwidth="2")


class Setari(tk.Frame):

    def __init__(self, parent, controller):

        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        tk.Frame.__init__(self, parent)

        self.TLabel1 = tk.Label(controller)
        self.TLabel1.place(relx=0.01, rely=0.965, height=19, width=185)
        self.TLabel1.configure(foreground="#000000")
        self.TLabel1.configure(font="TkDefaultFont")
        self.TLabel1.configure(relief="flat")
        self.TLabel1.configure(anchor='w')
        self.TLabel1.configure(justify='left')
        self.TLabel1.configure(text='''© 2020 Marius Odobașa  - ver 1.0''')

        self.controller = controller

        AgendaApp.rama(self, "Setări agendă telefonică")

        self.menubar = tk.Menu(
            controller, font="TkMenuFont", bg=_bgcolor, fg=_fgcolor)
        controller.configure(menu=self.menubar)

        self.sub_menu = tk.Menu(controller, tearoff=0)

        self.menubar.add_command(
            label="Administrare contacte",
            command=lambda: controller.show_frame("Administrare"))

        self.menubar.add_command(
            label="Setări",
            command=lambda: controller.show_frame("Setari"))

        self.menubar.add_command(
            label="Despre",
            command=lambda: controller.show_frame("Despre"))

        self.menubar.add_command(
            label="Ajutor",
            command=lambda: controller.show_frame("Ajutor"))

        self.menubar.add_command(
            label="Iesire",
            command=self.quit)


def multiple(*func_list):

    return lambda *args, **kw: [func(*args, **kw) for func in func_list]
    None


def scroll_to_view(scroll_set, *view_funcs):

    def closure(start, end):
        scroll_set(start, end)
        for func in view_funcs:
            func('moveto', start)
    return closure


class MultiListbox(tk.Frame):
    def __init__(self, master=None, columns=2, data=[], cautare=[], row_select=True, **kwargs):
        tk.Frame.__init__(self, master, borderwidth=1,
                          highlightthickness=1, relief=tk.SUNKEN)
        self.rowconfigure(1, weight=1)
        self.columns = columns
        agenda = Agenda()
        self.contacte = agenda.listeaza()
        self.cautare = agenda.listeaza_cautare()

        if isinstance(self.columns, (list, tuple)):
            for col, text in enumerate(self.columns):
                tk.Label(self, text=text).grid(row=0, column=col)
            self.columns = len(self.columns)

        self.boxes = []
        for col in range(self.columns):
            box = tk.Listbox(self, exportselection=not row_select, **kwargs)
            if row_select:
                box.bind('<<ListboxSelect>>', self.selected)
            box.grid(row=1, column=col, sticky='nsew')
            self.columnconfigure(col, weight=1)
            self.boxes.append(box)
        vsb = tk.Scrollbar(self, orient=tk.VERTICAL,
                           command=multiple(*[box.yview for box in self.boxes]))
        vsb.grid(row=1, column=col+1, sticky='ns')
        for box in self.boxes:
            box.config(yscrollcommand=scroll_to_view(vsb.set,
                                                     *[b.yview for b in self.boxes if b is not box]))

    def selected(self, event=None):
        row = event.widget.curselection()[0]
        for lbox in self.boxes:
            lbox.select_clear(0, tk.END)
            lbox.select_set(row)

    def cauta_data(self, cautare=[]):
        boxes = cycle(self.boxes)
        idx = -1
        for index in range(len(self.cautare)):
            for idx, (item, box) in enumerate(zip(self.cautare[index], boxes)):
                box.insert(tk.END, item)

            for _ in range(self.columns - idx % self.columns - 1):
                next(boxes).insert(tk.END, '')

    def add_data(self, data=[]):
        boxes = cycle(self.boxes)
        idx = -1
        for index in range(len(self.contacte)):
            for idx, (item, box) in enumerate(zip(self.contacte[index], boxes)):
                box.insert(tk.END, item)

            for _ in range(self.columns - idx % self.columns - 1):
                next(boxes).insert(tk.END, '')

    def __getitem__(self, index):
        '''get a row'''
        return [box.get(index) for box in self.boxes]

    def __delitem__(self, index):
        '''delete a row'''
        [box.delete(index) for box in self.boxes]

    def curselection(self):
        '''get the currently selected row'''
        selection = self.boxes[0].curselection()
        return selection[0] if selection else None

    def delete_curselection(self):
        agenda = Agenda()

        if self.curselection() == None:
            tk.messagebox.showinfo(
                'Ștergere', 'Nu ați selectat niciun contact!')
        else:
            selectat = self.__getitem__(self.curselection())
            nume = selectat[0] + " " + selectat[1]

            answer = tkinter.messagebox.askquestion(
                "Ștergere contact", "Ești sigur că vrei să ștergi contactul " + nume + "?", icon='warning')

            if answer == 'yes':
                selectat = self.curselection()
                self.__delitem__(self.curselection())
                agenda.sterge(str(selectat))

            elif answer == 'no':
                return


class Administrare(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        global lista_contacte
        global fereastra

        AgendaApp.rama(self, "Administrare contacte", xrelheight=0.09)
        Administrare.incarca_date()

        global cauta_input
        cauta_input = tk.Entry(self)
        cauta_input.place(relx=0.020, rely=0.065,
                          height=25, width=160, anchor="w")

        cauta = tk.Button(self, text="Caută", relief="ridge",
                          command=lambda: Cauta.cuvant())

        cauta.place(relx=0.190, rely=0.065, height=25, width=120, anchor="w")

        refresh = tk.Button(self, text="Actualizează",
                            relief="ridge", command=lambda: Adauga.actualizeaza())
        refresh.place(relx=0.324, rely=0.065,
                      height=25, width=120, anchor="w")

        adauga = tk.Button(self, text="Adaugă",
                           command=lambda: Adauga.fereastra(), relief="ridge")
        adauga.place(relx=0.58, rely=0.065,
                     height=25, width=120, anchor="w")

        modifica = tk.Button(self, text="Modifică",
                             command=lambda: Modifica.fereastra(), relief="ridge")
        modifica.place(relx=0.72, rely=0.065,
                       height=25, width=120, anchor="w")

        sterge = tk.Button(self, text="Șterge",
                           command=lambda: lista_contacte.delete_curselection(), relief="ridge")
        sterge.place(relx=0.86, rely=0.065,
                     height=25, width=120, anchor="w")

    def incarca_date():
        global lista_contacte
        global fereastra
        agenda = Agenda()
        agenda.incarc()
        fereastra = Frame()
        fereastra.place(relx=0.01, rely=0.535,
                        height=585, width=981, anchor="w", bordermode="outside")
        lista_contacte = MultiListbox(
            fereastra, ['Nume', 'Prenume', 'Telefon', 'Email'], width=5)
        lista_contacte.add_data()
        lista_contacte.pack(fill=tk.BOTH, expand=True)

    def actualizeaza():
        fereastra.destroy()
        Administrare.incarca_date()

    def email_valid(email):
        try:
            valid = validate_email(email)
            email = valid.email
            return True
        except EmailNotValidError as e:
            return False


class Adauga(Administrare):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

    def fereastra():
        global win
        global label_eroare
        win = tk.Toplevel()
        win.wm_title("Adaugă contact")
        win.geometry("400x250+760+365")
        win.iconbitmap('icon.ico')
        win.resizable(0, 0)

        label_nume = tk.Label(win, text="Nume: ")
        label_nume.place(relx=0.02, rely=0.120, anchor="w")

        label_prenume = tk.Label(win, text="Prenume: ")
        label_prenume.place(relx=0.02, rely=0.260, anchor="w")

        label_telefon = tk.Label(win, text="Telefon: ")
        label_telefon.place(relx=0.02, rely=0.400, anchor="w")

        label_email = tk.Label(win, text="Email: ")
        label_email.place(relx=0.02, rely=0.540, anchor="w")

        e_nume = tk.Entry(win)
        e_nume.place(relx=0.220, rely=0.120, height=25, width=300, anchor="w")

        e_prenume = tk.Entry(win)
        e_prenume.place(relx=0.220, rely=0.260,
                        height=25, width=300, anchor="w")

        e_telefon = tk.Entry(win)
        e_telefon.place(relx=0.220, rely=0.400,
                        height=25, width=300, anchor="w")

        e_email = tk.Entry(win)
        e_email.place(relx=0.220, rely=0.540, height=25, width=300, anchor="w")

        e_buton = tk.Button(win, text="Adaugă",
                            command=lambda: Adauga.adauga_date(e_nume, e_prenume, e_telefon, e_email))

        e_buton.place(relx=0.030, rely=0.780, height=30, width=378)

    def adauga_date(nume, prenume, telefon, email):

        agenda = Agenda()

        label_eroare = tk.Label(win, text="")
        label_eroare.place()
        label_eroare.config(text="")

        s_nume = nume.get()
        s_prenume = prenume.get()
        s_telefon = telefon.get()
        s_email = email.get()

        erori = True

        while erori == True:

            if (s_nume == "") or (s_prenume == "") or (s_telefon == "") or (s_email == ""):
                label_eroare.config(text="")

                label_eroare = tk.Label(win, text="Trebuie să completeazi toate câmpurile!", fg="red", font=(
                    "Helvetica Bold", 10, "bold"))
                label_eroare.place(x=win.winfo_width()/2 - 0.5,
                                   rely=0.700, anchor="center")
                erori = True
                return tk.Label(win, text="").place()

            elif not s_nume.isalpha():
                label_eroare.config(text="")

                label_eroare = tk.Label(win, text="    Trebuie sa introduci doar caractere    ", fg="red", font=(
                    "Helvetica Bold", 10, "bold"))
                label_eroare.place(x=win.winfo_width()/2 - 0.5,
                                   rely=0.700, anchor="center")
                erori = True
                return

            elif not s_prenume.isalpha():
                label_eroare.config(text="")

                label_eroare = tk.Label(win, text="     Trebuie sa introduci doar caractere     ", fg="red", font=(
                    "Helvetica Bold", 10, "bold"))
                label_eroare.place(x=win.winfo_width()/2 - 0.5,
                                   rely=0.700, anchor="center")
                erori = True
                return

            elif not Administrare.email_valid(s_email):
                label_eroare.config(text=" ")

                label_eroare = tk.Label(win, text="Adresă de email introdusă este invalidă", fg="red", font=(
                    "Helvetica Bold", 10, "bold"))
                label_eroare.place(x=win.winfo_width()/2 - 0.5,
                                   rely=0.700, anchor="center")
                erori = True
                return
            else:
                erori = False

        else:
            contact = Contact(s_nume, s_prenume, s_telefon, s_email)
            agenda.adauga(contact)
            win.destroy()
            Administrare.actualizeaza()


class Modifica(Administrare):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

    def fereastra():
        global win2
        agenda = Agenda()

        if lista_contacte.curselection() == None:
            tk.messagebox.showinfo(
                'Modifică', 'Nu ați selectat niciun contact!')
        else:
            selectat = lista_contacte.__getitem__(
                lista_contacte.curselection())

            nume = selectat[0] + " " + selectat[1]

            win2 = tk.Toplevel()
            win2.wm_title("Modifică contact")

            win2.geometry("400x250+760+365")
            win2.iconbitmap('icon.ico')
            win2.resizable(0, 0)

            label_nume = tk.Label(win2, text="Nume: ").place(
                relx=0.02, rely=0.120, anchor="w")
            label_prenume = tk.Label(win2, text="Prenume: ").place(
                relx=0.02, rely=0.260, anchor="w")
            label_telefon = tk.Label(win2, text="Telefon: ").place(
                relx=0.02, rely=0.400, anchor="w")
            label_email = tk.Label(win2, text="Email: ").place(
                relx=0.02, rely=0.540, anchor="w")

            nume = tk.Entry(win2, state="normal")
            nume.place(relx=0.220, rely=0.120,
                       height=25, width=300, anchor="w")

            prenume = tk.Entry(win2)
            prenume.place(relx=0.220, rely=0.260,
                          height=25, width=300, anchor="w")

            telefon = tk.Entry(win2)
            telefon.place(relx=0.220, rely=0.400,
                          height=25, width=300, anchor="w")

            email = tk.Entry(win2)
            email.place(relx=0.220, rely=0.540,
                        height=25, width=300, anchor="w")

            nume.insert(0, selectat[0])
            prenume.insert(0, selectat[1])
            telefon.insert(0, selectat[2])
            email.insert(0, selectat[3])

            buton = tk.Button(win2, text="Modifică",  command=lambda: Modifica.modifica_date(
                nume, prenume, telefon, email))
            buton.place(relx=0.030, rely=0.780, height=30, width=378)

    def modifica_date(nume, prenume, telefon, email):
        label_eroare = tk.Label(win2, text="")
        label_eroare.place()
        label_eroare.config(text="")

        s_nume = nume.get()
        s_prenume = prenume.get()
        s_telefon = telefon.get()
        s_email = email.get()

        erori = True

        while erori == True:

            if (s_nume == "") or (s_prenume == "") or (s_telefon == "") or (s_email == ""):
                label_eroare.config(text="")

                label_eroare = tk.Label(win2, text="Trebuie să completeazi toate câmpurile!", fg="red", font=(
                    "Helvetica Bold", 10, "bold"))
                label_eroare.place(x=win2.winfo_width()/2 - 0.5,
                                   rely=0.700, anchor="center")
                erori = True
                return tk.Label(win, text="").place()

            elif not s_nume.isalpha():
                label_eroare.config(text="")

                label_eroare = tk.Label(win2, text="    Trebuie sa introduci doar caractere    ", fg="red", font=(
                    "Helvetica Bold", 10, "bold"))
                label_eroare.place(x=win2.winfo_width()/2 - 0.5,
                                   rely=0.700, anchor="center")
                erori = True
                return

            elif not s_prenume.isalpha():
                label_eroare.config(text="")

                label_eroare = tk.Label(win2, text="     Trebuie sa introduci doar caractere     ", fg="red", font=(
                    "Helvetica Bold", 10, "bold"))
                label_eroare.place(x=win2.winfo_width()/2 - 0.5,
                                   rely=0.700, anchor="center")
                erori = True
                return

            elif not Administrare.email_valid(s_email):
                label_eroare.config(text=" ")

                label_eroare = tk.Label(win2, text="Adresă de email introdusă este invalidă", fg="red", font=(
                    "Helvetica Bold", 10, "bold"))
                label_eroare.place(x=win2.winfo_width()/2 - 0.5,
                                   rely=0.700, anchor="center")
                erori = True
                return
            else:
                erori = False

        else:
            agenda = Agenda()
            contact = Contact(s_nume, s_prenume, s_telefon, s_email)
            agenda.adauga(contact)
            agenda.sterge(lista_contacte.curselection())
            win2.destroy()
            Administrare.actualizeaza()


class Cauta(Administrare):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        AgendaApp.rama(self, "Despre")

    def cuvant():
        agenda = Agenda()
        cuvant = cauta_input.get()

        win3 = tk.Toplevel()
        win3.wm_title('Căutare contact după: %s' % str(cuvant))
        win3.geometry("960x600+450+220")
        win3.iconbitmap('icon.ico')
        win3.resizable(0, 0)

        agenda = Agenda()
        date = agenda.cauta(nume=cuvant)

        lista_cautare = MultiListbox(
            win3, ['Nume', 'Prenume', 'Telefon', 'Email'], width=5)

        lista_cautare.cauta_data()
        lista_cautare.pack(fill=tk.BOTH, expand=True)


class Despre(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        AgendaApp.rama(self, "Despre")


class Ajutor(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        AgendaApp.rama(self, "Ajutor")


def main():
    app = AgendaApp()
    app.title('Agendă telefonică')

    windowWidth = app.winfo_reqwidth()
    windowHeight = app.winfo_reqheight()

    positionRight = int(app.winfo_screenwidth()/2 - windowWidth/2)
    positionDown = int(app.winfo_screenheight()/2 - windowHeight/2)

    app.geometry("1000x700+" + str(int(positionRight/2)) +
                 "+" + str(int(positionDown/3)))

    app.iconbitmap('icon.ico')
    app.resizable(0, 0)
    app.mainloop()


if __name__ == "__main__":
    main()
