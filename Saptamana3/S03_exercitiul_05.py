# Exercițiul 5
# Scrieti un program care va elimina dintr-un dictionar cheile care au valori duplicate Exemplu: {'a': 1, 'b': 1, 'c': 1, 'd': 2} -> {'d': 2}

dictionar = {
    'a': 1,
    'b': 1,
    'c': 1,
    'd': 2,
    'f': 1,
    'g': 2,
    'h': 3,
    'g': 2,
    'i': 4,
    'j': 5,
    'k': 5
}

solutie = {
    'h': 3,
    'i': 4
}

rezultat = {}

for key, value in dictionar.items():
    if list(dictionar.values()).count(value) == 1:
        rezultat[key] = value

if rezultat == solutie:
    print("Felicitări! {} este răspunsul corect!". format(rezultat))
else:
    print(rezultat)
    print("Eroare: Implementare greșită!")
