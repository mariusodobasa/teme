# Exercițiul 4
# Scrieti un program care citeste de la tastatura un numar N si construieste un dictionar cu urmatoarele proprietati:
# dictionarul are N chei fiecare cheie este un numar de la [0, N) valoarea unei chei (k) este o lista de forma: [ < True/False > daca numarul este prim sau nu,
# < suma numerelor pana la k, inclusiv k > ]

istoric = [
    'STÂNGA', 2,
    'JOS', 2,
    'DREAPTA', 5,
]
print(istoric)

i = 0
x = 0
y = 0

while i < len(istoric):
    directie = istoric[i]
    pas = istoric[i+1]

    if directie == 'STÂNGA':
        x = x - (pas)
    if directie == 'DREAPTA':
        x = pas + x

    if directie == 'JOS':
        y = y - (pas)
    if directie == 'SUS':
        y = pas + y

    i += 2

pozitie_cursor = []
pozitie_cursor.append(x)
pozitie_cursor.append(y)
print('Pozitia cursorului este: ', pozitie_cursor)
