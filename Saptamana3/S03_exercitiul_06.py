# Exercitiul 6

# Tuxy vrea sa aleaga cel mai bun cercetator din tara. Pentru a face asta a organizat alegeri in fiecare laboator si a
# primit rezultatele locale, acum are nevoie de ajutorul tau sa afle rezultatele finale. Tuxy are un dictionar de forma:

# rezultate_locale = {
# 'laboator iasi': {
#     'Cercetator 1': 128,
#     'Cercetator 2': 150
#     'Cercetator 2': 94,
#    ....
#     }
# 'laboator bucuresti': {
#     'Cercetator 3': 281,
#     'Cercetator 4': 224
#     'Cercetator 10': 412,
#     ....
#     }
# }
# El are nevoie de un dictionar care ii spune numarul total de voturi adunate de un cercetator.

rezultate_locale = {

    'laboator iasi': {
        'Cercetator 1': 128,
        'Cercetator 2': 150,
        'Cercetator 3': 94,
        'Cercetator 4': 110,
    },
    'laboator bucuresti': {
        'Cercetator 1': 281,
        'Cercetator 2': 224,
        'Cercetator 3': 412,
        'Cercetator 4': 65,
    }
}

print("Cercetatorul 1 are:", rezultate_locale.get('laboator iasi').get(
    'Cercetator 1') + rezultate_locale.get('laboator bucuresti').get('Cercetator 1'), "voturi")
print("Cercetatorul 2 are:", rezultate_locale.get('laboator iasi').get(
    'Cercetator 2') + rezultate_locale.get('laboator bucuresti').get('Cercetator 2'), "voturi")
print("Cercetatorul 3 are:", rezultate_locale.get('laboator iasi').get(
    'Cercetator 3') + rezultate_locale.get('laboator bucuresti').get('Cercetator 3'), "voturi")
print("Cercetatorul 4 are:", rezultate_locale.get('laboator iasi').get(
    'Cercetator 4') + rezultate_locale.get('laboator bucuresti').get('Cercetator 4'), "voturi")

