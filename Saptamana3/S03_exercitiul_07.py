# Exercitiul 7
#
# Tuxy trebuie sa cumpere echipamente pentru laborator. A primit doua dictionare, unul cu echipamentele ce trebuie cumparate si cantiatea lor,
# si unul cu preturile echipamentelor (pentru o singura unitate). Ar vrea sa stie care e costul total.

pret_pe_unitate = {
    'Monitor': 300,
    'Scaun': 250,
    'Unitate_de_calcul': 1500,
    'Unitate_grafica': 8000,
}
necesar_pentru_laborator = {
    'Monitor': 1,
    'Scaun': 4,
    'Unitate_de_calcul': 200,
    'Unitate_grafica': 50
}

cost_total = 0

for key, value in necesar_pentru_laborator.items():
    pret_reper = pret_pe_unitate.get(key, 0)

    if not pret_reper:
        continue
    print("Produs: {} - {} x {} = {}".format(
        key, value, pret_reper, value * pret_reper))
    cost_total = cost_total + pret_reper * value

print("Prețul total al echipamentelor este: {}".format(cost_total))
