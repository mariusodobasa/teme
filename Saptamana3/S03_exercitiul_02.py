# Exercițiul 2
#
# Creati un program care combina doua dictionare in unul singur ?
# Aveti grija ce se intampla cand ambele au valori pentru aceeasi cheie.
# #############################
# In cazul unui conflict :
# - daca ambele valori sunt int se va pastra suma lor {'k': 10} + {'k':20} => {'k': 30}
# - daca ambele valori sunt float se va pastra media lor {'k': 10.0} + {'k':20.0} => {'k': 15.0}
# - daca ambele valori sunt string se va pastra stringul care le contine pe amandoua cu un spatiu
#   lor {'k': "aaa"} + {'k': "bbbb"} => {'k': "aaa bbbb"}
# - daca ambele sunt liste atunci rezultatul va fi compus din toate elementele ordonate
#   {'k': [1, 2, 3]} + {'k': [2, 3, 4]} => {'k': [1, 2, 2, 3, 3, 4]}
# - daca ambele sunt set-uri atunci vom alege doar intersectia
#   {'k': {1, 2, 3}} + {'k': {2, 3, 4}} => {'k': {2, 3}}
# - daca ambele sunt bool atunci vom avea valoarea de adevar a operatiei or
#   {'k': True} + {'k': False} => {'k': True}
# - daca tipurile nu sunt la fel, atunci vor avea un tuple  {'k6': 10.0} + 'k6': 'Y', => {'k6': (10,0, 'Y')}

# Exemplu complex:
d1 = {
    'k1': 1,
    'k2': 2,
    'k3': 30.0,
    'k4': 'aaa',
    'k5': 10,
    'k6': 10.0,
    'k7': 10,
    'k8': 'aaa',
    'k9': [1, 2, 3],
    'k10': {1, 2, 3},
    'k11': True,
}
d2 = {
    'k2': 20,
    'k3': 30.0,
    'k4': 'bbbb',
    'k5': 'X',
    'k6': 'Y',
    'k7': 10,
    'k8': 'aaa',
    'k9': [2, 3, 4],
    'k10': {2, 3, 4},
    'k11': False,
    'k100': 'TEXT',
}

# Asteptam rezultatul:
rezultat_asteptat = {
    'k1': 1,  # exita doar in d1
    'k2': 22,  # ambele sunt int 2 + 20
    'k3': 30.0,  # ambele sunt float (30.0+30.0)/2
    'k4': 'aaa bbbb',  # ambele sunt string
    'k5': (10, 'X'),  # tipurile sunt diferit, ambele valori in tuple
    'k6': (10.0, 'Y'),  # tipuri de date diferite, ambele valori in tuple
    'k7': 20,  # ambele sunt int 10+10
    'k8': 'aaa aaa',  # ambele sunt string
    'k9': [1, 2, 2, 3, 3, 4],  # ambele sunt liste: le extidem si sortam
    'k10': {2, 3},  # ambele sunt seturi atunci pastram doar intersectia
    'k11': True,  # ambele sunt bool atunci facem operatia or intre valori
    'k100': 'TEXT',  # exista doar in d2
}
# Hint #1: isinstance(val, int), isinstance(val, str), isinstance(val, float)
# Hint #2: nu mai exista regula "daca doua elemente sunt la fel -> se pastreaza doar unul" deoarece daca doua elemente
#          sunt la fel, atunci sunt si de acelasi tip
# Hint #3: aveti grija AMBELE elemente trebuie sa fie de acel tip, nu doar unul dintre ele (verificati cazurile k5 si k6 de exemplu
# Hint #4: incepeti cu cazul cel mai simplu, de exemplu daca ambele sunt int si daca tipurile de date difera

new_d = d1.copy()  # d1
for key, value in d2.items():  # k100, text
    # conflict
    if key in new_d:
        if isinstance(d1[key], bool) and isinstance(d2[key], bool):
            new_d[key] = d1[key] or d2[key]

        elif isinstance(d1[key], int) and isinstance(d2[key], int):
            new_d[key] = d1[key] + d2[key]

        elif isinstance(d1[key], float) and isinstance(d2[key], float):
            new_d[key] = (d1[key] + d2[key])/2

        elif isinstance(d1[key], str) and isinstance(d2[key], str):
            new_d[key] = d1[key] + " " + d2[key]

        elif isinstance(d1[key], list) and isinstance(d2[key], list):
            # sorted > returneaza o lista
            # .sort >
            # aux = d1[key][:] + d2[key]
            # aux.sort()
            # new_d[key] = aux
            new_d[key] = sorted(d1[key] + d2[key])

        elif isinstance(d1[key], set) and isinstance(d2[key], set):
            new_d[key] = d1[key].intersection(d2[key])

        else:
            new_d[key] = (d1[key], d2[key])
    else:
        # se afla in d2 dar nu si in d1
        new_d[key] = value
print("{}\n{}\n{}\n{} ".format(d1, d2, new_d, rezultat_asteptat))

if new_d == rezultat_asteptat:
    print("[OK] Felicitari, functioneaza ! (momentan)")
else:
    print("[EROARE] Implementare gresita!")
