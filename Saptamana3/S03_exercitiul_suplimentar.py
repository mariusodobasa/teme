'''Tuxy are nevoie de o agenda de telefon, ar avea nevoie de un program interactiv care ii permite sa faca urmatoarele actiuni:

sa adauge un contact nou in agenta(nume si numar de telefon)
sa afiseze toate contactele
sa afiseze un contact anume
sa stearga un contact anume
sa afiseze toate contactele care incep cu un prefix'''

def print_menu():
    print("1. Afișare agendă telefonică")
    print("2. Adaugă contact nou")
    print("3. Sterge contact")
    print("4. Caută contact")
    print("5. Ieșire")
    print()


numbers = {}
menu_choice = 0

print_menu()
while menu_choice != 5:
    menu_choice = int(input("Alege opțiune (1-5): "))
    if menu_choice == 1:
        if len(numbers) == 0:
            print("\nAtenție! Agenda nu conține nici un contact")
            input("\nApasă enter pentru a continua!")
            print_menu()
        else:
            print("Agendă:")
            for x in numbers.keys():
                print("Nume: ", x, "\tNumăr:", numbers[x])
            input("\nApasă enter pentru a continua!")
            input()
            print_menu()
    elif menu_choice == 2:
        print("Adaugă nume și număr de telefon: ")
        name = input("Nume: ")
        phone = input("Număr de telefon: ")
        numbers[name] = phone
        print("A fost adăugat în agenda contactul:\n {} - {} \n".format(name, phone))
        input("\nApasă enter pentru a continua!")
        print_menu()
    elif menu_choice == 3:
        print("Șterge contact din agendă")
        name = input("Nume: ")
        if name in numbers:
            del numbers[name]
        else:
            print(name, "nu a fost găsit")
    elif menu_choice == 4:
        print("Caută contact")
        name = input("Nume: ")
        if name in numbers:
            print("Numărul de telefon este: ", numbers[name])
        else:
            print(name, "nu a fost găsit")
    elif menu_choice != 5:
        print_menu()
