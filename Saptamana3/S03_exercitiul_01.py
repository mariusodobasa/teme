# Exercițiul 1
# Implementati transformarea unei liste de tuple intr-un dictionar fara a folosi dict.
# Exemplu: [("prima valoare", 1), ("a doua valoare", 2)] -> {'prima valoare': 1, 'a doua valoare': 2

l = [("prima valoare", 1), ("a doua valoare", 2)]
d = {}
for item in l:
    d[item[0]] = item[1]
print("d=", d)
