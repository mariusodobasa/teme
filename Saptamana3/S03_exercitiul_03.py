# Exercițiul 3
# Scrieti un program care numara aparitiile cuvintelor intr-un text si le afiseaza la ecran pe 5 cele mai folosite cuvinte.

text = "aaa aaa bbb bbb bbb ddd ddd ddd ddd ddd vvv eee eee eee eee eee eee ggg ggg ggg aaa aaa bbb eee ooo ooo ooo iii iii uuu uuu mmm nnn nnn mmm mmm jjj jjj ddd zzz zzz eee zzz lll lll lll 333"

cuvinte = text.split()
dictionar = {}

for i in cuvinte:
    if i not in dictionar:
        dictionar[i] = 0
    dictionar[i] += 1

a = {k: v for k, v in sorted(dictionar.items(), key=lambda item: item[1])}

b = dict()
for key, value in dictionar.items():
    b.setdefault(value, list()).append(key)

c = dict(sorted(b.items()))

for key, value in c.items():
    for i in range(len(c)):
        print("Apariții {} data/ori: {}".format(key, value))
        break

print("\n")
print("Lista celor mai folosite 5 cuvite: {}".format(
    sorted(c.items())[len(c)-5:len(c)+5]))
