# Exercitiul 8
# Organizaţia Internaţională a Aviaţiei Civile propune un alfabet în care fiecărei litere îi este asignat un
# cuvânt pentru a evita problemele în înțelegerea mesajelor critice.

# Pentru a se păstra un istoric al conversațiilor s-a decis transcrierea lor conform următoarelor reguli:
# - fiecare cuvânt este scris pe o singură linie
# - literele din alfabet sunt separate de o virgulă

# Avem nevoie de doua programe, unul de criptare si unul de decriptare care vor primi ca input textul.

# Alfabetul ICAO este urmatorul:

ICAO = {
    'a': 'alfa', 'b': 'bravo', 'c': 'charlie', 'd': 'delta', 'e': 'echo',
    'f': 'foxtrot', 'g': 'golf', 'h': 'hotel', 'i': 'india', 'j': 'juliett',
    'k': 'kilo', 'l': 'lima', 'm': 'mike', 'n': 'november', 'o': 'oscar',
    'p': 'papa', 'q': 'quebec', 'r': 'romeo', 's': 'sierra', 't': 'tango',
    'u': 'uniform', 'v': 'victor', 'w': 'whiskey', 'x': 'x-ray', 'y': 'yankee',
    'z': 'zulu'
}

text = input("Introdu textul: ")


def split(s, sep=' '):
    return [s[:s.index(sep)]] + split(s[s.index(sep)+1:]) if sep in s else [s]


def transform(cuvant):
    for item in cuvant:
        for k, v in ICAO.items():
            if item == k:
                print(v, end=" ")
    print("")


cuvinte = split(text)

for i in range(len(cuvinte)):
    transform(cuvinte[i])
